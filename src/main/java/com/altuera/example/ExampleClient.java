
package com.altuera.example;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.leader.LeaderSelectorListenerAdapter;
import org.apache.curator.framework.recipes.leader.LeaderSelector;
import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
// apache
import org.apache.curator.utils.ThreadUtils;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ExecutorService;
// logger
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

class ExampleClient extends LeaderSelectorListenerAdapter implements Closeable
{
  private final static Logger logger = LoggerFactory.getLogger(ExampleClient.class);
  
  final AtomicInteger callCount;
  final AtomicInteger leaderCount;
  
  ThreadFactory defaultThreadFactory;  
  ExecutorService executor;
  
  LeaderSelector leaderSelector;
  
  CuratorFramework client;
  String name;
  
  public ExampleClient(
    CuratorFramework client,
    String path,
    String name
  ) {
      
     this.client = client;
     this.name = name;
  
    callCount = new AtomicInteger();
    leaderCount = new AtomicInteger();
    
    defaultThreadFactory = ThreadUtils.newThreadFactory("LeaderSelector-test");
    executor = Executors.newFixedThreadPool(2);

    // create a leader selector using the given path for management
    // all participants in a given leader selection must use the same path
    // ExampleClient here is also a LeaderSelectorListener but this isn't required
        
    leaderSelector = new LeaderSelector(
      client, 
      path, 
      new CloseableExecutorServiceTest(
        Executors.newSingleThreadExecutor(defaultThreadFactory), 
        true,
        this
      ),
      this);

    // for most cases you will want your instance to requeue when it relinquishes leadership
    leaderSelector.autoRequeue();
  
  }

  void start() throws IOException
  {
      // the selection for this instance doesn't start until the leader selector is started
      // leader selection is done in the background so this call to leaderSelector.start() returns immediately
      leaderSelector.start();
  }

  @Override
  public void close() throws IOException
  {
      leaderSelector.close();
  }

  @Override
  public void takeLeadership(CuratorFramework client)
  {
    // we are now the leader. This method should not return until we want to relinquish leadership

    int waitSeconds = (int)(5 * Math.random() + 1);

    logger.info(name + " is now the leader. Waiting " + waitSeconds + " seconds...");
    logger.info(name + " has been leader " + leaderCount.getAndIncrement() + " time(s) before.");
    try
    {
      Thread.sleep(TimeUnit.SECONDS.toMillis(waitSeconds));
    }
    catch(InterruptedException e) {
      logger.info(name + " was interrupted.");
      Thread.currentThread().interrupt();      
    }
    finally
    {
      logger.info(name + " relinquishing leadership.\n");
    }
  }
  
  void interruptLeadership() {  
    leaderSelector.interruptLeadership();
  }
  
  void correctInterruptLeadership() {  
    if( leaderSelector.hasLeadership() )
    {
      leaderSelector.interruptLeadership();
    }
  }
  
  public void submitCallback() throws InterruptedException {
    try {
        int count = callCount.getAndIncrement();
        logger.info("Submit task was called #" + count);
        
        // if it will interrupt only once then it is okey
        // if( count == 3 )
        if( 3 <= count && count <= 4 )
        {      
          executor.submit(() -> {
            try {
              Thread.sleep(10);
              
              logger.info("Call again interruptLeadership");
              // If we simply interrupt leadership then autoRequeue stop working correctly
              interruptLeadership();
              
              // If we interrupt correctly then problem disappears
              // correctInterruptLeadership()
            }
            catch(InterruptedException e) {
              logger.error("Thread was interrupted", e);
            }
          });
          
          Thread.sleep(250);
        }
    }
    catch(InterruptedException e) {
        throw e;
    }
  }
}
