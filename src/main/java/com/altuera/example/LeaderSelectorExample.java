
package com.altuera.example;

import com.google.common.collect.Lists;
import org.apache.curator.utils.CloseableUtils;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.test.TestingServer;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
// logger
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
// zookeper
import org.apache.zookeeper.WatchedEvent;
import org.apache.curator.RetryPolicy;

import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

class LeaderSelectorExample {
  private static final Logger logger = LoggerFactory.getLogger(LeaderSelectorExample.class);
  public static final String PATH = "/examples/leader";
    
  public static void main(String[] args) throws Exception
  {
  
    String connectString = "localhost:2181";
    RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
           
    logger.info("Notice that leader election is fair: all clients will become leader and will do so the same number of times.");

    TestingServer server = new TestingServer();
    
    CuratorFramework client = CuratorFrameworkFactory.newClient(
      server.getConnectString(), 
      retryPolicy);

    
    ExampleClient example = new ExampleClient(client, PATH, "Client #" + 1);
    
    try
    {
      client.start();
      example.start();

      logger.info("Press enter/return to close application\n");
      new BufferedReader(new InputStreamReader(System.in)).readLine();
    }
    finally
    {
      logger.info("Shutting down...");

      CloseableUtils.closeQuietly(example);
      CloseableUtils.closeQuietly(client);

      CloseableUtils.closeQuietly(server);
    }
  }
    
}
