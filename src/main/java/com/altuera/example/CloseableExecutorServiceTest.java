package com.altuera.example;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.Callable;
// curator
import org.apache.curator.utils.CloseableExecutorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class CloseableExecutorServiceTest extends CloseableExecutorService {
  private final static Logger logger = LoggerFactory.getLogger(ExampleClient.class);

  ExampleClient client;

  public CloseableExecutorServiceTest(ExecutorService executorService, boolean shutdownOnClose, ExampleClient client) {
    super(executorService, shutdownOnClose);

    this.client = client;
  }

  @Override
  public <V> Future<V> submit(Callable<V> task) {
    try {
      client.submitCallback();
    } catch (InterruptedException e) {
      logger.error("Thread was interrupted", e);
    }
    return super.submit(task);
  }
}
