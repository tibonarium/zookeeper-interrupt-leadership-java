Curator. LeaderSelector. Two successive calls to interruptLeadership() will break autoRequeue
=============================================================================================

If we set autoRequeue to TRUE. But during execution interruptLeadership() will be called from another thread before internalRequeue() completed its work. Then it will break recursive call to internalRequeue(), so that client will not ask for leadership and get stuck.

We can solve this problem if we check hasLeadership() before calling interruptLeadership(). But it is strange that such check curator library does not do internally.


